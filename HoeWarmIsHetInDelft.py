#!/usr/bin/env python

from bs4 import BeautifulSoup
from selenium import webdriver
options = webdriver.ChromeOptions()
options.add_argument('headless')
driver = webdriver.Chrome(options=options)


def get_temperature(target_url):
    # The weather widget is stored within an iframe named ifrm_3.
    # Get content using Selenium.
    driver.get(target_url)
    driver.switch_to.frame("ifrm_3")
    iframe = driver.page_source

    # Now use bs4 to get the raw temperature string
    iframe_content = BeautifulSoup(iframe, "html.parser")
    temp_string = iframe_content.find("span", id="ajaxtemp").text.strip()

    # Remove last two characters of temp string and
    # then round to nearest degree and print out temperature.
    temp_string = temp_string[:-2]
    temperature = round(float(temp_string))
    print("%d degrees Celsius" % (temperature))


# Run function to get current temperature from weerindelft.nl
get_temperature("https://www.weerindelft.nl")
