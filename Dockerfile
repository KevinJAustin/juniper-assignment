FROM selenium/standalone-chrome:latest
LABEL authors="Kevin Austin (kevin@kevops.co.uk)"

COPY requirements.txt /opt/app/requirements.txt
COPY HoeWarmIsHetInDelft.py /opt/app/HoeWarmIsHetInDelft.py
WORKDIR /opt/app

RUN sudo apt-get update -y
RUN sudo apt-get install python3-pip -y
RUN sudo pip3 install -r requirements.txt

CMD [ "python3", "./HoeWarmIsHetInDelft.py" ]
