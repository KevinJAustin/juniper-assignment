# The Current Temperature in Delft

This is a test assignment which will get the current temperature in Delft from www.weerindelft.nl.

## Getting Started

If you look at the web site, you can see that the weather string itself is embedded within an iframe, which in turn is populated by JavaScript.  Because of this, using a Python library like BeautifulSoup is not possible. (As this is dynamic content)

I have therefore decided that the most robust solution is to obtain the dynamic content using Selenium.  If you're not familiar with Selenium, it's typically used as a browser testing tool, but this purpose we will be using it to grab the dynamic content from the web site.

### Prerequisites

This assignment is integrated with GitLab CI.  The GitLab file is simple in nature and performs the following steps:

* Pulls a standalone Chrome Docker image (provided by Selenium);
* Install pip3 on the image;
* Installs Python dependencies (there are only three);
* Performs static analysis against the Python code;
* Runs the Python script to obtain the temperature.

### Installing

Clone this repository and enter the directory:

```
git clone git@gitlab.com:KevinJAustin/juniper-assignment.git
cd juniper-assignment
```

You will need to create a new project within GitLab.  When this is created you can then push this code to your repository with the following:

```
git push git@gitlab.com:[YOUR-USERNAME]/[YOUR-REPO].git
```

On push, GitLab will kick off the CI process.  The 'Deploy' stage will show the temperature similar to the screenshot below.

![GitLab Screenshot](gitlab-screenshot.png)

## Docker Image

You can create a Docker container from the Dockerfile:

```
docker build -t delft .
```

To run the container:

```
docker run delft
```

The Docker image has also been uploaded to [Docker Hub](https://cloud.docker.com/repository/docker/kevinjaustin/juniper-assignment).  You can pull and run this image using this command:

```
docker run kevinjaustin/juniper-assignment
```

## Suggsted Improvements

The following list suggests improvements that could be made to this process:

* Obtain the temperature information from an API such as OpenWeatherMap;
* Integrate unit tests (using the unittest framework);
* When tests are successful to build and tag our Docker image;
* Push the Docker image to a registry (ECR, Docker Hub etc.);
* Use this image within GitLab CI to improve execution time.


## Authors

* **Kevin Austin** - [KevinJAustin](https://github.com/KevinJAustin)
